# história & violência

## wikipedia
- <https://en.wikipedia.org/wiki/List_of_wars_and_anthropogenic_disasters_by_death_toll>

### primeira guerra mundial:
  + aliados
    Military dead: 5,525,000
    Civilian dead: 4,000,000
  + central
    Military dead: 4,386,000
    Civilian dead: 3,700,000

### segunda guerra mundial:
  + aliados
    Military dead: Over 16,000,000
    Civilian dead: Over 45,000,000
    Total dead: Over 61,000,000
    (1937–1945)
  + eixo
    Military dead: Over 8,000,000
    Civilian dead: Over 4,000,000
    Total dead: Over 12,000,000
    (1937–1945)


### cuba
- revolução cubana (1953–1959)
    mortos: mais de 5 mil cubanos ligados ao combate (?)
    5,000+ combat-related deaths; unknown thousands of dissidents arrested and murdered by Batista's government; unknown number of people executed by the Rebel Army
- rebelião de escambray (1960-1966)
  + rebeldes com apoio norte-americano
    2,000-3,000 killed
  + cubanos
    Armed Forces: 500 soldiers killed
    National Militia: 3,500+ militia killed
- 1994: 41 Cubans who attempted to leave the island of Cuba on a hijacked tugboat drowned at sea
- em 2003, 75 ativistas da oposição foram presos e condenados a vários anos na prisão

### urss
- guerra civil/revolução russa (1917-1922)
  + exército vermelho
    Casualties and losses
    ~1,500,000
    259,213 killed
    60,059 missing
    616,605 died of disease/wounds
    3,878 died in accidents/suicides
    548,857 wounded/frostbitten
  + exército branco
    650,000 total casualties
  + white Terror
    Estimates for those killed in the White Terror vary, from between 20,000 and 100,000 people as well as much higher estimates of 300,000 deaths.
  + red Terror
    Estimates for the total number of victims of Bolshevik repression vary widely. One source asserts that the total number of victims of repression and pacification campaigns could be 1.3 million, whereas another gives estimates of 28,000 executions per year from December 1917 to February 1922. The most reliable estimations for the total number of killings put the number at about 100,000, whereas others suggest a figure of 200,000.
  + coletivização (1928-1933)
    Recent historians have estimated the death toll in the range of six to 13 million.
  + great purge (1936–1938)
    Estimates of the number of deaths associated with the Great Purge run from the official figure of 681,692 to nearly 1 million.
  + great famine (1932–33)
    estimative: 6 to 8 million people died from hunger in the Soviet Union during this period, of whom 4 to 5 million were Ukrainians
- stalin/total de mortes propositais
  In 2011, the historian Timothy D. Snyder, after assessing 20 years of historical research in Eastern European archives, asserts that Stalin deliberately killed about 6 million (rising to 9 million if foreseeable deaths arising from policies are taken into account)

### repúplica popular da china
- revolução chinesa
  república da china: c. 1.5 million (1948–49)
  partido comunista: c. 250,000 (1948–49)
  totais:
    1945–49: c. 6 million (including civilians)
    1928–37: c. 7 million (including civilians)
    1945–49: c. 2.5 million (including civilians)
- revolução cultural (1966-1976)
   Estimates range upwards to several millions, but an estimate of around 400,000 deaths is a widely accepted minimum figure.
   MacFarquhar and Schoenhals assert that in rural China alone some 36 million people were persecuted, of whom between 750,000 and 1.5 million were killed, with roughly the same number permanently injured.
   Jung Chang and Jon Halliday claim that as many as 3 million people died in the violence of the Cultural Revolution.
   The Holocaust memorial museum puts the death toll between 5 and 10 million.

### coreia do norte
- 1994–1998: North Korean famine.
  Scholars estimate 600,000 died of starvation (other estimates range from 200,000 to 3.5 million).

### 9/11
- 2,996 people

### guerra do golfo
  + Coalition:
    292 killed (147 killed by enemy action, 145 non-hostile deaths)
    467 wounded in action
    776 wounded
  + Kuwait:
    4,200 killed
    12,000 captured
  + Iraqi:
    25,000–50,000 killed
    75,000+ wounded
    80,000 captured
  + Kuwaiti civilian losses:
    Over 1,000 killed
    600 missing people
  + Iraqi civilian losses:
    About 3,664 killed
  + Other civilian losses:
    300 civilians killed, more injured

### guerra no iraque
  + Scientific surveys:
    Iraq Family Health Survey - 151,000 violent deaths - March 2003 to June 2006
    Lancet survey - 601,027 violent deaths out of 654,965 excess deaths - March 2003 to June 2006
    PLOS Medicine Survey - 460,000 deaths in Iraq as direct or indirect result of the war including more than 60% of deaths directly attributable to violence - March 2003 to June 2011
  + Body counts:
    Associated Press - 110,600 violent deaths. -	March 2003 to April 2009
    Iraq Body Count project -	183,535–206,107 civilian deaths from violence. - March 2003 to April 2019
    Classified Iraq War Logs - 109,032 deaths including 66,081 civilian deaths. - January 2004 to December 2009

### guerra no afeganistão
  + Afghan security forces:
    62,000+ killed
  + Northern Alliance:
    200 killed
  + Coalition
    Dead: 3,561
    (United States: 2,419, United Kingdom: 456, Canada: 158, France: 89, Germany: 57, Italy: 53, Others: 321)
    Wounded: 22,773 (United States: 19,950, United Kingdom: 2,188, Canada: 635)
  + Contractors
    Dead: 3,937
    Wounded: 15,000+
    Total killed: 69,698+ killed
+ Taliban: 60,000–65,000+ killed
+ al-Qaeda: 2,000+ killed
+ ISIL–KP: 2,400+ killed
+ Civilians killed: 38,480+

### segunda guerra do congo (1998–2004)
- 2.7 million people died, mostly from starvation and disease

### grandes fomes
- fome no yemen (2016-presente)
  Total deaths: More than 85,000 children (adults unknown)
  Death rate: At least 130 children (adults unknown) per day (December 2016–November 2017 estimate)
- fome na somália (2011-2012)
  285,000
- fome no sudão durante a guerra em darfur (2003–2005)
  200,000
- (1984–1985): Famine caused by drought, economic crisis and the Second Sudanese Civil War
  240,000
- 1983–1985 1983–1985 famine in Ethiopia
  400,000–600,000
- 1982–1985 Famine caused by the Mozambican Civil War / Mozambique
  100,000
- 1980–1981	Caused by drought and conflict / Uganda
  30,000
- 1975–1979	Khmer Rouge, Cambodia
  1,500,000–2,000,000
- 1974 	Bangladesh famine of 1974
  27,000-1,500,000
- 1968–1972 Sahel drought created a famine that killed a million people: Mauritania, Mali, Chad, Niger and Burkina Faso
  1,000,000
- 1967–1970 Biafran famine caused by Nigerian blockade
  2,000,000
- 1966–1967 Lombok, drought and malnutrition, exacerbated by restrictions on regional rice trade in Indonesia
  50,000

### incidentes no mar mediterrâneo com barcos de imigrantes (2015)
- Deaths: Over 1200 (estimated; 35 confirmed)
- Missing	450

### guerra civil na síria
- estimativa atual: 535,000

### guerra contra as drogas no méxico
- 115,000 deaths from organized crime homicides 2007–2018

## totais até agora

- 1ª guerra: 17 milhões
- 2ª guerra: 73 milhões (Hitler: 45 milhões de civis, 61 milhões no total)

- socialismo (cuba, urss, china):
  - conflitos: ~25 milhões
  - fome: ~20 milhões
- capitalismo (contagem parcial/grandes acontecimentos):
  - conflitos: ~3,2 milhões
  - fome: ~8,9 milhões
