
# CONFIG

## wacom no openbox

map:

`list devices`

`xsetwacom set 11 MapToOutput HEAD-1`

mais info: <https://wiki.archlinux.org/index.php/Wacom_tablet#Adjusting_aspect_ratios>

# NOTAS DE REINSTALAÇÃO DO SISTEMA | abril/2019

-   backup ppas
-   npm hexo + extensões
-   pip: mailmerge
-   tema arc darker modificado

## flatpak apps

-   flatpak install --assumeyes --user "flathub" "com.github.gijsgoudzwaard.image-optimizer"
-   flatpak install --assumeyes --user "flathub" "com.github.muriloventuroso.easyssh"
-   flatpak install --assumeyes --user "flathub" "com.github.paolostivanin.OTPClient"
-   flatpak install --assumeyes --user "flathub" "com.meetfranz.Franz"
-   flatpak install --assumeyes --user "flathub" "com.skype.Client"
-   flatpak install --assumeyes --user "flathub" "de.wolfvollprecht.UberWriter"
-   flatpak install --assumeyes --user "flathub" "io.webtorrent.WebTorrent"
-   flatpak install --assumeyes --user "flathub" "org.gnome.FeedReader"
-   flatpak install --assumeyes --user "flathub" "org.gottcode.FocusWriter"
-   flatpak install --assumeyes --user "flathub" "org.gnome.glabels-3"
-   flatpak install --assumeyes --user "flathub" "org.gottcode.NovProg"
-   flatpak install --assumeyes --user "flathub" "org.kde.kdenlive"
-   flatpak install --assumeyes --user "flathub" "org.telegram.desktop"
-   flatpak install --assumeyes --user "flathub" "net.supertuxkart.SuperTuxKart"
-   flatpak install --assumeyes --user "flathub" "org.flarerpg.Flare"
-   flatpak install --assumeyes --user "flathub" "org.megaglest.MegaGlest"
-   flatpak install --assumeyes --user "flathub" "org.speed_dreams.SpeedDreams"
-   flatpak install --assumeyes --user "flathub" "org.tuxfamily.StuntRally"
-   org.gtk.Gtk3theme.Adapta-Eta
-   org.gtk.Gtk3theme.Adapta-Nokto-Eta
-   org.gtk.Gtk3theme.Adapta-Nokto
-   org.gtk.Gtk3theme.Adapta
-   org.gtk.Gtk3theme.Arc-Dark-solid
-   org.gtk.Gtk3theme.Arc-Dark
-   org.gtk.Gtk3theme.Arc-Darker-solid
-   org.gtk.Gtk3theme.Arc-Darker
-   org.gtk.Gtk3theme.Arc-solid
-   org.gtk.Gtk3theme.Arc

## snap apps

-   briss              0.9-snap4                   3     stable    mihan1235     -
-   core               16-2.39                     6964  beta      canonical✓    core
-   core18             20190409                    941   stable    canonical✓    base
-   gtk-common-themes  0.1-16-g2287c87             1198  stable    canonical✓    -
-   hashit             3.4.4                       324   stable    javadsm       -
-   hugo               0.55.5                      4624  stable    hugo-authors  -
-   rambox             0.6.7                       5     stable    ramboxapp✓    -
-   scrcpy             v1.8-14-ge443518            61    edge      sisco311      -

## local

-   bleachbit
-   gksu
-   gnome-appfolders-manager
-   gnome-kra-ora
-   guake 3.4
-   hugo_extended
-   manuskript
-   pandoc 2.7.1
-   warsaw
-   zettlr

## extra

-   `sudo add-apt-repository ppa:teejee2008/ppa`
-   `sudo add-apt-repository ppa:webupd8team/y-ppa-manager`
-   `sudo add-apt-repository ppa:graphics-drivers/ppa`
-   `sudo add-apt-repository ppa:fish-shell/release-3`

## printer & hplip

Migrate Linux Printer Configuration

CUPS stores its configuration at /etc/cups directory, so all you have to do is copy /etc/cups to a new computer. Open terminal and type the commands on old Linux computer:
`tar -cvzf /tmp/cups-$(hostname).tar.gz /etc/cups`

Copy `/tmp/cups_` to new system using SCP or use USB pen driver:
`scp /tmp/cups_ new.linux.server.com:/tmp`

Now login to new system and type the following commands:

`mv /etc/cups /etc/cups.backup`

`cd /`

`tar -zxvf /tmp/cups*`

Finally, restart the cups service:

`/etc/init.d/cupsys restart`

## config

-   ! certbot
-   remove gdm3
-   calibre (script site)
-   install lightdm
-   gnome extensions / helper
-   <https://tunwinnaing.wordpress.com/2015/10/04/conky-configuration-new-setting-syntax/>
-   mover temas/icons para $HOME

## var/server

-   backup dos arquivos
-   backup apache conf
