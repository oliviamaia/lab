## /haus

-   [webmin](https://192.168.0.102:10000/)
-   [transmission](http://192.168.0.102:9091/transmission/web/)
-   [sickgear](http://192.168.0.102:8081/)
-   [couchpotato](http://192.168.0.102:5050/)
-   [roteador](http://192.168.0.1/)
-   [impressora](http://192.168.0.103)

## /psl

-   [syncthing](http://localhost:8384)
-   [nextcloud](https://192.168.0.102/nextcloud/)
-   [livros](https://192.168.0.102/livros)
-   [disroot;email](https://mail.disroot.org/)
-   [mailbox;email](https://office.mailbox.org)

## /hub

-   [.net](https://oliviamaia.net/)
-   [rabiscolab](https://lab.oliviamaia.net)
-   [gitlab](https://gitlab.com/)
-   [github](https://github.com/)
-   [netlify](https://app.netlify.com)
-   [heroku](https://dashboard.heroku.com)
-   [gdomains](https://domains.google/#/)
-   [mailgun](https://www.mailgun.com/)

## /ppl

-   [twitter](https://tweetdeck.twitter.com/)
-   [mastodon](https://mastodon.social/web/)
-   [pixelfed](https://pixelfed.social/)
-   [instagram](https://instagram.com/_nyex)
-   [pinterest](https://br.pinterest.com/)
-   [goodreads](https://www.goodreads.com/)
-   [csurfing](https://www.couchsurfing.org/profile.html)

## /ftw

-   [news](https://192.168.0.102/nextcloud/index.php/apps/news/)
-   [hackernews](https://news.ycombinator.com/)
-   [producthunt](https://www.producthunt.com/)
-   [bubblinnews](https://news.bubblin.io/)
-   [reddit](https://reddit.com)
-   [unsplash](https://unsplash.com/)
-   [dotshare](http://dotshare.it/dots/)
-   [feedi](https://search.feedi.me/)

## /$

-   [pagseguro](https://pagseguro.uol.com.br/index.jhtml)
-   [paypal](https://www.paypal.com.br/)
-   [itaú](https://www.itaupersonnalite.com.br/)
-   [MEI](https://www.portaldoempreendedor.gov.br)
-   [apoia.se](https://apoia.se/rabiscologia)
-   [catarse](https://www.catarse.me/tregua)
-   [amazon kdp](https://kdp.amazon.com)
-   [kobo wl](https://writinglife.kobobooks.com)

## /media

-   [jellyfin](http://192.168.0.102:8096)
-   [netflix](https://www.netflix.com)
-   [youtube](https://invidio.us/)

## /tls

-   [imgur](https://olimaia.imgur.com/)
-   [firefox send](https://send.firefox.com/)
-   [inky](http://cakedown.alexandredeschamps.ca/)
-   [pastebin](https://sharppaste.nl/)
-   [browserpad](http://browserpad.org/)
-   [dillinger](https://dillinger.io/)
-   [colordrop](https://colordrop.io/)
-   [colorhunt](https://colorhunt.co)

## /ref

-   [indexed](https://awesome-indexed.mathew-davies.co.uk/)
-   [piracy](https://github.com/Igglybuff/awesome-piracy)
-   [self-hosted](https://github.com/Kickball/awesome-selfhosted)
-   [linux apps](https://wiki.archlinux.org/index.php/List_of_applications)
-   [fontawesome](https://fontawesome.com/icons?d=gallery)
-   [alternativeto](https://alternativeto.net/)
-   [slant](https://www.slant.co/)

## /code

-   [codeacademy](https://www.codecademy.com)
-   [freecodecamp](https://learn.freecodecamp.org/)
-   [sololearn](https://www.sololearn.com/)
-   [coursera](https://www.coursera.org/)
-   [edX](https://courses.edx.org)
-   [codepen](https://codepen.io)
-   [codesandbox](https://codesandbox.io/)
-   [sequential](https://sequential.js.org/)

## /dl

-   [mobilism](https://forum.mobilism.org/)
-   [filepursuit](https://filepursuit.com/)
-   [libgen](http://libgen.io/)
