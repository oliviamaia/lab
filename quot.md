# quotes etc

## Harvey

> At the end of the day, capitalists care only about the surplus value, which will be realised as monetary profit. They are indifferent as to the particular commodities they produce. If there is a market for poison gas then they will produce it.
>
> Workers in general get a declining share in total national income but now have mobile phones and tablets. Meanwhile, the top 1 per cent take an ever increasing share of total value output. This is not, as Marx is at pains to point out, a law of nature but without any counterforce this is what capital does.
>
> Through the creation of indebtedness, which includes by the way the creation of money by the banks independent entirely of value production, the field of distribution internalises a tremendous incentive to perpetuate circulation through valorisation. It is not impossible to say that the incentive to redeem debts plays just as important a role in impelling future value production as the search for profit. Debts are claims upon future value production and, as such, foreclose upon the future of valorisation. Failure to redeem debts initiates the mother of all crises to the system of capital flow.
>
> Finally the law which always holds the relative surplus population or industrial reserve army in equilibrium with the extent and energy of accumulation rivets the worker to capital more firmly than the wedges of Hephaestus held Prometheus to the rock. It makes the accumulation of misery a necessary condition, corresponding to the accumulation of wealth. Accumulation of wealth at one pole is, therefore, at the same time accumulation of misery, the torment of labour, slavery, ignorance, brutalisation and moral degradation at the opposite pole, i.e. on the side of the class that produces its own product as capital.
>
> David Harvey

---

## Borges sobre _A invenção de Morel_:

> "He discutido con su autor los pormenores de su trama, la he leído; no me parece una imprecisión o una hipérbole calificarla de perfecta."

---

## Galeano

"Al fin y al cabo, somos lo que hacemos para cambiar lo que somos" - Eduardo Galeano

---

## Growth

"Growth for the sake of growth is the ideology of the cancer cell" - Edward Abbey

---

## Sleep

> When action grows unprofitable, gather information; when information grows unprofitable, sleep.
>
> Ursula K. Le Guin, _The Left Hand of Darkness_
