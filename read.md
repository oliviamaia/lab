# Leituras

→ <https://outline.com>

# Arquivo

## Ref

-   [i3: i3 User’s Guide](https://i3wm.org/docs/userguide.html#_using_i3)
-   [How to Add Javascript to Hugo • Tejas Gupta](https://tgnyc.github.io/posts/addjstohugo/)
-   [Grav Development with GitHub - Part 1 | Official home of Grav CMS](https://getgrav.org/blog/developing-with-github-part-1)
-   [Let’s Talk About Margins — by Craig Mod](https://craigmod.com/essays/lets_talk_about_margins/)
-   [Cintiqs on GNU/Linux: How to setup brightness, contrast and more. - David Revoy](https://www.davidrevoy.com/article710/cintiqs-on-gnu-linux-how-to-setup-brightness-contrast-and-more)
-   [Roll your own Linux desktop using Openbox • Daniel Wayne Armstrong](https://www.circuidipity.com/openbox/)
-   [Building a Custom Linux Environment With Openbox | John Ramsden](https://ramsdenj.com/2016/03/28/building-a-custom-linux-environment-with-openbox.html)
-   [Self-Host Your Static Assets – CSS Wizardry – CSS Architecture, Web Performance Optimisation, and more, by Harry Roberts](https://csswizardry.com/2019/05/self-host-your-static-assets/)
-   [GitHub - jlevy/the-art-of-command-line: Master the command line, in one page](https://github.com/jlevy/the-art-of-command-line)
-   [The fish Shell](http://mvolkmann.github.io/fish-article/)
-   [The Fish Shell](https://flaviocopes.com/fish-shell/)
-   [The fish shell is awesome - Julia Evans](https://jvns.ca/blog/2017/04/23/the-fish-shell-is-awesome/)
-   [The Difference Between HTML And HTML5: Comprehensive Guide](https://www.bitdegree.org/tutorials/difference-between-html-and-html5/)
-   [O JavaScript que você deveria conhecer. – Pedro Polisenso – Medium](https://medium.com/@pedropolisenso/o-javasscript-que-voc%C3%AA-deveria-conhecer-b70e94d1d706)
-   [Read JavaScript Allongé (ES5) | Leanpub](https://leanpub.com/javascript-allonge/read)
-   [PHP: The Right Way](https://phptherightway.com/)
-   [Adapticons - Apps on Google Play](https://play.google.com/store/apps/details?id=pl.damianpiwowarski.adapticons)
-   [Custom Quick Settings - Apps on Google Play](https://play.google.com/store/apps/details?id=com.quinny898.app.customquicksettings)
-   [GitHub - relink2013/Awesome-Self-hosting-for-the-whole-family: An Awesome List of apps that can be self hosted that your family can actually use without frustration.](https://github.com/relink2013/Awesome-Self-hosting-for-the-whole-family)
-   [N. / startpage · GitLab](https://gitlab.com/Seiber/startpage)
-   [Top 10 Best Free WordPress Themes for Beginner Writers and Bloggers](https://www.kevinmuldoon.com/best-free-wordpress-themes-beginner-writers-bloggers/)
-   [Quer dar pausas no uso do computador? Instale o Take a Break](https://www.edivaldobrito.com.br/quer-dar-pausas-no-uso-do-computador-instale-o-take-a-break/)
-   [Cómo crear aplicaciones web de escritorio con Firefox](https://www.muylinux.com/2019/02/28/aplicaciones-web-de-escritorio-firefox/)
-   [4 Linux Automation Apps to Streamline Your Tasks and Workflows](https://www.makeuseof.com/tag/linux-automation-apps/)
-   [My Personal App Recommendations for All Android Users (New and Old)](https://www.reddit.com/r/androidapps/comments/av0p2g/my_personal_app_recommendations_for_all_android/)
-   [Install Android 8.1 Oreo on Linux To Run Apps & Games](https://fosspost.org/tutorials/install-android-8-1-oreo-on-linux)
-   [GitHub - fossasia/phimpme-android: Phimp.me - Android Photo Image Picture Editor App https://phimp.me](https://github.com/fossasia/phimpme-android)
-   [GitHub - ukanth/afwall: AFWall+ (Android Firewall +) - iptables based firewall for Android](https://github.com/ukanth/afwall)

## Artigos

-   [Can a Party Drug Really Treat Severe Depression?](https://www.thecut.com/2019/03/fda-approves-ketamine-nasal-spray-to-treat-depression.html?utm_source=nym&utm_medium=f1&utm_campaign=feed-part)
-   [Shamed for Taking Bipolar Disorder Medication | The Mighty](https://themighty.com/2019/02/health-privilege-psychiatric-medication/)
-   [Why data, not privacy, is the real danger](https://www.nbcnews.com/business/business-news/why-data-not-privacy-real-danger-n966621)


-   [That Deprecated Linux Machine: Sound Volume Management Through Multimedia Keys in Openbox](https://thatdeprecatedlinuxmachine.blogspot.com/2013/07/sound-volume-management-through.html?m=1)
-   [The Self-Serving Myths of Silicon Valley](https://jacobinmag.com/2018/11/live-work-die-silicon-valley-review-pein/)
-   [It Is Highly Unlikely That Any of This Exists: On the Origins of the Universe | Literary Hub](https://lithub.com/it-is-highly-unlikely-that-any-of-this-exists-on-the-origins-of-the-universe/)
-   [“Marx’s Refusal of the Labour Theory of Value” by David Harvey - Reading Marx's Capital with David Harvey](http://davidharvey.org/2018/03/marxs-refusal-of-the-labour-theory-of-value-by-david-harvey/)


-   [Facebook is a capitalism problem, not a Mark Zuckerberg problem](https://www.vox.com/recode/2019/5/10/18563895/facebook-chris-hughes-mark-zuckerberg-break-up-monopoly)
-   [How the dualism of Descartes ruined our mental health](https://aeon.co/ideas/how-the-dualism-of-descartes-ruined-our-mental-health?utm_medium=feed&utm_source=rss-feed)
-   [Avengers and the Endgame of Liberalism](http://thephilosophicalsalon.com/avengers-and-the-endgame-of-liberalism/)
-   [Static Websites Generators Compared](https://blog.logrocket.com/the-best-static-websites-generators-compared-5f1f9eeeaf1a)
-   [Create an Hexo Theme - Part 2: Other Pages - CodeBlocQ](http://www.codeblocq.com/2016/03/Create-an-Hexo-Theme-Part-2-Other-Pages/)
-   [Create an Hexo Theme - Part 3: Comments, Analytics and Widgets - CodeBlocQ](http://www.codeblocq.com/2016/03/Create-an-Hexo-Theme-Part-3-Comments-Analytics-and-Widgets/)
-   [Create an Hexo Theme - Part 1: Index - CodeBlocQ](http://www.codeblocq.com/2016/03/Create-an-Hexo-Theme-Part-1-Index/)
-   [How to build an Android app from source ⋅ Plume](https://fediverse.blog/~/SylkeViciousThinks/how-to-build-an-android-app-from-source)
-   [Grav Development with GitHub - Part 2 | Official home of Grav CMS](https://getgrav.org/blog/developing-with-github-part-2)
-   [GitHub - k88hudson/git-flight-rules: Flight rules for git](https://github.com/k88hudson/git-flight-rules)
-   [A belief in meritocracy is not only false: it’s morally wrong](https://aeon.co/ideas/a-belief-in-meritocracy-is-not-only-false-its-morally-wrong?utm_medium=feed&utm_source=rss-feed)
-   [Can a philosopher explain reality and make-believe to a child? | Aeon Essays](https://aeon.co/essays/can-a-philosopher-explain-reality-and-make-believe-to-a-child?utm_medium=feed&utm_source=rss-feed)
-   [Book covers still use the phrase “A Novel” for works of fiction - Vox](https://www.vox.com/the-goods/2019/2/14/18223954/a-novel-book-cover-reading-line)
-   [Forget privacy: you're terrible at targeting anyway - apenwarr](https://apenwarr.ca/log/20190201)
-   [Opinion | In Praise of Mediocrity - The New York Times](https://www.nytimes.com/2018/09/29/opinion/sunday/in-praise-of-mediocrity.html)
-   [RE: Software disenchantment - Rakhim.org](https://rakhim.org/2018/09/re-software-disenchantment/)
-   [Vladimir Nabokov Taught Me How to Be a Feminist – Electric Literature](https://electricliterature.com/vladimir-nabokov-taught-me-how-to-be-a-feminist-229f3dbade6f?source=rss----e470410858b4---4&gi=cbc927cc65ee)

## Tech

-   [Read Oh My JS | Leanpub](https://web.archive.org/web/20150317231950/https://leanpub.com/ohmyjs/read)
-   [JavaScript plus a dash of JQuery](http://nicholasjohnson.com/javascript-book/)
-   [Conheça o AppImageHub, um repositório central de apps AppImage](https://www.edivaldobrito.com.br/conheca-o-appimagehub/)

## Newsletter

-   [A Manifesto for Opting Out of an Internet-Dominated World - The New York Times](https://www.nytimes.com/2019/04/30/books/review/jenny-odell-how-to-do-nothing.html?utm_source=hackernewsletter&utm_medium=email&utm_term=books)
-   [Against cheerfulness](https://aeon.co/essays/cheerfulness-cannot-be-compulsory-whatever-the-t-shirts-say?utm_medium=feed&utm_source=rss-feed)
-   [Opinion | Against ‘Sustainability’ - The New York Times](https://www.nytimes.com/2016/08/08/opinion/against-sustainability.html)
-   [We are all Digital Slaves](https://hackernoon.com/we-are-all-digital-slaves-5fb04c5a7d0f?source=rss----3a8144eabfe3---4)
-   [A Manifesto for Opting Out of an Internet-Dominated World](https://www.nytimes.com/2019/04/30/books/review/jenny-odell-how-to-do-nothing.html)
-   [Daddy Issues | Amber A’Lee Frost](https://thebaffler.com/all-tomorrows-parties/daddy-issues-frost)
-   [I Feel Better Now | Jake Bittle](https://thebaffler.com/latest/i-feel-better-now-bittle?utm_source%3Drss-feed%26utm_medium%3Drss%26utm_campaign%3Dfeed)
-   [In defence of antidepressants](https://aeon.co/essays/why-the-constant-trashing-of-antidepressants-is-absurd)
-   [If the Point of Capitalism is to Escape Capitalism, Then What’s the Point of Capitalism?](https://eand.co/if-the-point-of-capitalism-is-to-escape-capitalism-then-whats-the-point-of-capitalism-bedd1b2447d?gi=3e87e7bb7c89)
-   [Marxism and Buddhism](https://aeon.co/essays/how-marxism-and-buddhism-complement-each-other)
- [How “Carpe Diem” Got Lost in Translation](https://daily.jstor.org/how-carpe-diem-got-lost-in-translation/)
